<?php

namespace Drupal\webform_octoa\Plugin\WebformHandler;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;


/**
 * Webform submission remote post handler.
 *
 * @WebformHandler(
 *   id = "octoa_lead",
 *   label = @Translation("Octoa Lead Post"),
 *   category = @Translation("External"),
 *   description = @Translation("Posts webform submissions to a Octoa Lead Form."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class OctoaWebformHandler extends WebformHandlerBase {

    /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    WebformSubmissionConditionsValidatorInterface $conditions_validator,
    ClientInterface $http_client,
    WebformElementManagerInterface $element_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->httpClient = $http_client;
    $this->elementManager = $element_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('http_client'),
      $container->get('plugin.manager.webform.element')
    );
  }

   /**
    * {@inheritdoc}
    */
  public function defaultConfiguration() {
    return [
      'octoa_form_id' => '',
      'octoa_field_ids' => array(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();

    $form['octoa_form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The remote form id'),
      '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['octoa_form_id'],
    ];

    $form['octoa_field_ids'] = array(
      '#type' => 'table',
      '#header' => [$this->t('Field Name'), $this->t('Octoa Field ID')],
      '#empty' => $this->t('There are no items yet'),
    );

    $elements = $webform->getElementsDecodedAndFlattened();
    $ignore_types = [
      'webform_flexbox',
      'webform_actions',
    ];

    foreach ($elements as $id => $data) {
      // Some table columns containing raw markup.
      if (!in_array($data['#type'], $ignore_types)) {
        $form['octoa_field_ids'][$id]['field_name'] = array(
          '#plain_text' => $data['#title'],
        );
        $form['octoa_field_ids'][$id]['octoa_field_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Octoa Field ID'),
          '#size' => 6,
          '#default_value' => $this->configuration['octoa_field_ids'][$id]['octoa_field_id'],
          '#title_display' => 'hidden',
        ];
      }
    }

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $octoaConfig = $this->configFactory->get('webform_octoa.settings');
    if (empty($octoaConfig->get('api_url'))
      || empty($octoaConfig->get('secret'))
      || empty($this->configuration['octoa_form_id'])
      || empty($this->configuration['octoa_field_ids'])) {

      return;
    }

    $data = $webform_submission->getData();
    $form_data = [];
    foreach ($this->configuration['octoa_field_ids'] as $field_name => $config) {
      if (!empty($data[$field_name])) {
        $form_data[] = [
          'id' => $config['octoa_field_id'],
          'value' => $data[$field_name],
        ];
      }
    }

    $request_options = [
      'headers' => [
        'X-API-KEY' => $octoaConfig->get('secret'),
        'Content-Type' => 'application/json',
      ],
      'json' => [
        'form' => [
          'fields' => $form_data,
        ],
      ]
    ];
    $this->httpClient->request('POST', rtrim($octoaConfig->get('api_url'), '/') . '/v1/external/create/lead/' . $this->configuration['octoa_form_id'], $request_options);
  }
}
