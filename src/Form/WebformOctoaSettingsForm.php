<?php

namespace Drupal\webform_octoa\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Webform Octoa settings for this site.
 */
class WebformOctoaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_octoa_webform_octoa_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_octoa.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#required' => TRUE,
      '#default_value' => $this->config('webform_octoa.settings')->get('api_url'),
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret'),
      '#required' => TRUE,
      '#default_value' => $this->config('webform_octoa.settings')->get('secret'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('webform_octoa.settings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('secret', $form_state->getValue('secret'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
